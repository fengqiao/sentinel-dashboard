#!/bin/bash

java -Djava.security.egd=file:/dev/./urandom \
`#-Dspring.cloud.sentinel.datasource.nacos.server-addr=127.0.0.1:8848 `\
`#-Dspring.cloud.sentinel.datasource.nacos.groupId=SENTINEL_GROUP `\
`#-Dspring.cloud.sentinel.datasource.nacos.namespace= `\
`#-Dspring.cloud.sentinel.datasource.nacos.flow-data-postfix=-flow-rules `\
`#-Dspring.cloud.sentinel.datasource.nacos.degrade-data-postfix=-degrade-rules `\
`#-Dspring.cloud.sentinel.datasource.nacos.system-data-postfix=-system-rules `\
`#-Dspring.cloud.sentinel.datasource.nacos.param-flow-data-postfix=-param-flow-rules `\
`#-Dspring.cloud.sentinel.datasource.nacos.authority-data-postfix=-authority-rules `\
`#-Dspring.cloud.sentinel.datasource.nacos.check-count=6 `\
`#-Dserver.port=8858 `\
`#-Dauth.enabled="true" `\
`#-Dsentinel.dashboard.auth.username=sentinel `\
`#-Dsentinel.dashboard.auth.password=sentinel `\
`#-Dserver.servlet.session.timeout=7200 `\
`#-Dcsp.sentinel.dashboard.server=localhost:8858 `\
`#-Dproject.name=sentinel `\
`#-Dcsp.sentinel.api.port=8719 `\
-jar /sentinel/sentinel-dashboard.jar