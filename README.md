# Sentinel 控制台整合 nacos  
## 版本: 1.7.1  

## 1. 已提交记录为概述 (五次)  
###### 1.1、初始化(b2550307): 下载1.7.1对应版本代码, 导入模块 sentinel-dashboard, 上传到git </br> 源码地址:https://github.com/alibaba/Sentinel/releases  
###### 1.2、sentinel整合nacos, 实现数据持久化(4522635c,27296f38): 这个就不详细解释了, 网上真的太多了,搜索一下sentinel整合nacos, 都是这一步. </br> Sentinel-1.7.1\sentinel-dashboard\src\test\java\com\alibaba\csp\sentinel\dashboard\rule\nacos是官方给的整合nacos的案列, 太简洁了, 故需进行修改完善  
###### 1.3、处理sentinel持久化nacos(3e87e56a): 发布配置nacos异步处理, 导致数据不一致, 以及间接导致问题 </br> 如: CUD页面刷新后,数据需在次刷新才能同步数据, 否在次添加会导致数据覆盖等问题  
###### 1.4、修改nacos参数为可配置(a8e05ecd,4890a4ff): 提取nacos参数为可配置项, 以及处理默认值, 可通过entrypoint.sh进行挂载指定  
###### 1.5、去除缓存repository(7994ab97),修改接口入参,以及页面: repository没有存在的必要, CURD都是操作的nacos, 对应的参数进行修改  
###### 1.6、后面的提交就是完善一下代码, 添加一下部署相关操作  
![README_1.png](README_1.png)  

## 2. 简单说一下前端js修改  
#### 1. 首先需安装node, 执行对应的命令: npm install, npm start [README.md](./src/main/webapp/resources/README.md)  
#### 2. npm-shrinkwrap.json这个文件一定得有, 好像是处理版本不一致导致(找前端老大哥帮忙处理的, 我这渣渣也是不太明白)  
#### 3. 程序最终执行的是dist文件夹里面编译后的代码, 而不是app文件夹里面的js代码  
#### 4. npm start会编译好文件覆盖掉旧的dist里面的文件  

## 3. 打包docker镜像  
#### 1. 可以参考我以前的一篇博客: https://blog.csdn.net/qq_38637558/article/details/105154483  

## 4. 除官方指定的参数, 还可以指定的参数(整合nacos后可整合参数)  
###### ***=默认值(所列值皆为默认值)  
#### 1.-Dspring.cloud.sentinel.datasource.nacos.server-addr=127.0.0.1:8848 &emsp;&emsp; (nacos地址)  
#### 2.-Dspring.cloud.sentinel.datasource.nacos.groupId=SENTINEL_GROUP &emsp;&emsp; (nacos分组)  
#### 3.-Dspring.cloud.sentinel.datasource.nacos.namespace= &emsp;&emsp; (nacos命名空间, 默认值空)  
#### 4.-Dspring.cloud.sentinel.datasource.nacos.flow-data-postfix = -flow-rules &emsp;&emsp; (流控规则nacos配置文件后缀)  
#### 5.-Dspring.cloud.sentinel.datasource.nacos.degrade-data-postfix = -degrade-rules &emsp;&emsp; (降级规则nacos配置文件后缀)  
#### 6.-Dspring.cloud.sentinel.datasource.nacos.system-data-postfix = -system-rules &emsp;&emsp; (系统规则nacos配置文件后缀)  
#### 7.-Dspring.cloud.sentinel.datasource.nacos.param-flow-data-postfix = -param-flow-rules &emsp;&emsp; (热点规则nacos配置文件后缀)  
#### 8.-Dspring.cloud.sentinel.datasource.nacos.authority-data-postfix = -authority-rules &emsp;&emsp; (授权规则nacos配置文件后缀)  
#### 9.-Dspring.cloud.sentinel.datasource.nacos.check-count = 6 &emsp;&emsp; (nacos发布配置等待监听响应时长: check-count * 500ms.系统稳定状态配置6已足够)  
#### 10.-Dserver.port=8858 &emsp;&emsp; (项目端口)  

## 5. 微服务接入方式  
#### 1. 添加依赖: <dependency><groupId>com.alibaba.csp</groupId><artifactId>sentinel-datasource-nacos</artifactId></dependency>  
![README_2.png](README_2.png)  

## 6.更多  
#### 1. 启动配置项: https://github.com/alibaba/Sentinel/wiki/%E5%90%AF%E5%8A%A8%E9%85%8D%E7%BD%AE%E9%A1%B9  
#### 2. 控制台: https://github.com/alibaba/Sentinel/wiki/%E6%8E%A7%E5%88%B6%E5%8F%B0  
#### 3. 对应博客地址: https://blog.csdn.net/qq_38637558/article/details/121369135  
#### 3. 镜像地址: https://hub.docker.com/r/chaim2436/sentinel-dashboard  
