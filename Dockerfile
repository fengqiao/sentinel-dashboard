#  基础镜像必要，代表你的项目将构建在这个基础上面
FROM java:8
# 维护者名字
MAINTAINER Chaim
# 声明服务运行在8858端口
EXPOSE 8858
# 创建一个可以从本地主机或其他容器挂载的挂载点，一般用来存放数据库和需要保持的数据等
# time zone 采用docker默认时区 UTC
#RUN echo "Asia/Shanghai" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata
# 执行命令，创建文件夹
# 将jar包添加到容器中并更名为 sentinel-dashboard.jar
COPY target/sentinel-dashboard.jar sentinel/sentinel-dashboard.jar
COPY entrypoint.sh sentinel/entrypoint.sh
# 在容器构建过程中需要在/目录下创建一个jarsentinel-dashboard.jar文件
RUN bash -c 'touch /sentinel/sentinel-dashboard.jar'
RUN chmod +x sentinel/entrypoint.sh
# 指定容器启动文件
ENTRYPOINT ["./sentinel/entrypoint.sh"]
